这个小程序商城基于框架ThinkPHP5最新版5.0.10实现，代码都附有注释，采用OOP思想编写后端api接口，后端cms位于public目录下，和服务器端分离，单独拿出去重新配域名即可，解决了js跨域问题，小程序前端代码位于public目录下的phone目录，移植到微信开发者工具就能运行，有几个需要修改的地方：
application目录下的extra文件修改自己的域名地址
setting.php中
```
'img_prefix'=>'你的域名/images',
'pay_back_url'=>'你的域名/v1/pay/notify',
'access_token_url'=>微信获取AccessToken的接口
```
在wx.php中
```
'app_id'=>'你的app_id',
'app_secret'=>'你的app_secret',
```
上面两个登录微信小程序在开发者设置中获取

在extend目录下的WxPay中的WxPay.config.php中配置自己的
 
* APPID：绑定支付的APPID（必须配置，开户邮件中可查看）
* MCHID：商户号（必须配置，开户邮件中可查看）
* KEY：商户支付密钥，参考开户邮件设置（必须配置，登录商户平台自行设置）
* 设置地址：https://pay.weixin.qq.com/index.php/account/api_cert
* APPSECRET：公众帐号secert（仅JSAPI支付的时候需要配置， 登录公众平台，进入开发者中心可设置），
* 获取地址：https://mp.weixin.qq.com/advanced/advanced?action=dev&t=advanced/dev&token=2005451881&lang=zh_CN

```
const APPID = 'wxae68dc18cc016c73';
const MCHID = '1900009851';
const KEY = '8934e7d15453e97507ef794cf7b0519d';
const APPSECRET = '7813490da6f1265e4901ffb80afaa36f';
```
 **配置这个是为了能够支付，如果不做支付可以不配置** 

除此之外需要在微信小程序前端代码中的utils目录下的config中配置自己的接口地址
```
Config.restUrl = '你的域名/api/v1/';
```
