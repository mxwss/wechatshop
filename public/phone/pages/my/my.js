// my.js
import {My} from "../my/my-model.js";
import { Address } from "../../utils/address.js";
import { Order } from "../order/order-model.js";
var my = new My();
var order = new Order();
var address = new Address();
Page({

  /**
   * 页面的初始数据
   */
  data: {
  pageIndex:1,
  orderArr:[],
  isLoadedAll:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this._loadData();
    this._getAddressInfo();
    
  },

  onShow:function(){
    var flag = order.hasNewOrder();
    if(flag){
      this.refresh();
    }
  },
  //切换tap栏时根据缓存的标志位newOrder决定是否更新订单信息
  refresh:function(){
   var that = this;
   this.data.orderArr = [];
   this._getOrders(()=>{
     that.data.isLoadedAll = false;
     that.data.pageIndex = 1;
     order.execSetStorageSync(false);
   });

  },
  //获取用户地址
  _getAddressInfo:function(){
    address.getAddress((res)=>{
        this.setData({
          addressInfo:res
        });
    });
  },
  //获取用户头像即名字信息
  _loadData:function(){
    my.getUserInfos((res)=>{
      this.setData({
        userInfo:res
      });
    }); 
    this._getOrders();
  },

  //加载历史订单信息
  _getOrders:function(callback){
    order.getOrders(this.data.pageIndex,(res)=>{
      var data = res.data;
      if(data.length>0){
        this.data.orderArr.push.apply(this.data.orderArr,data);
        this.setData({
          orderArr: this.data.orderArr
        });
      }else{
        this.data.isLoadedAll = true;
      }
      callback && callback();
    });
  },
  //用户查看历史订单拉到底部触发的事件
  onReachBottom:function(){
    if(!this.data.isLoadedAll){
      this.data.pageIndex++;
      this._loadOrders();
    }
  },

  //订单详情
  showOrderDetailInfo:function(event){
    var id = order.getDataSet(event,'id');
    wx.navigateTo({
      url: '../order/order?from=order&id='+id,
    })
  },

  //个人中心的支付
  rePay:function(event){
    var id = order.getDataSet(event,'id'),
        index = order.getDataSet(event,'index');
    this._execPay(id,index);
  },

  _execPay:function(id,index){
    var that = this;
    order.execPay(id,(res)=>{
      if(res>0){
        var flag = res == 2;
        if(flag){
          that.data.orderArr[index].status = 2;
          that.setData({
            orderArr:that.data.orderArr
          });
        }
        wx.navigateTo({
          url: '../pay-result/pay-result?id='+id+'&flag='+flag+'&from=my',
        });
      }else
      {
        wx.showModal({
          title: '支付失败',
          content: '商品下架',
          showCancel:false,
          success:function(res){}
        })
      }
    });
  },
  //用户添加收货地址事件
  editAddress: function (event) {
    var that = this;
    wx.chooseAddress({
      success: function (res) {
        let detailInfo = {
          name: res.userName,
          mobile: res.telNumber,
          totalDetail: address.setAddressInfo(res)
        };
        that.setData({
            addressInfo: detailInfo
          });
        address.submitAddress(res, (flag) => {
          if (!flag) {
            wx.showModal({
              title: '操作提示',
              content: '地址更新失败',
              showCancel:false,
              success:function(){}
            });
          }
        });
      }
    });
  }
})