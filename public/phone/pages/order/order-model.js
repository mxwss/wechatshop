import { Base } from '../../utils/base.js';
class Order extends Base{
  constructor(){
    super();
    this._storageKeyName = 'newOrder';
  }

  //下订单
  doOrder(param,callback){
      var that = this;
      var allParams = {
        url:'order',
        type:'POST',
        data:{
          products:param
        },
        sCallBack:function(res){
          that.execSetStorageSync(true);
          callback && callback(res);
        },
        eCallback:function(res){
        }
      };
      this.request(allParams);
  }

  /**
   * 从服务器获取订单快照信息
   */
  getOrderInfoById(id,callback){
    var params = {
      url:'order/'+id,
      sCallback:function(res){
        callback && callback(res);
      },
      eCallback:function(){}
    };
    this.request(params);
  }
  /**
   * 拉起微信支付
   * callback 0表示商品缺货导致支付失败，1表示用户取消支付或者支付失败，2支付成功
   */
  execPay(id,callback){
    let allParams = {
      url:'pay/pre_order',
      type:'POST',
      data:{
        id:id
      },
      sCallback:function(res){
        let timeStamp = res.timeStamp;
        if (timeStamp){
          wx.requestPayment({
            timeStamp: timeStamp.toString(),
            nonceStr: res.nonceStr,
            package: res.package,
            signType: res.signType,
            paySign: res.paySign,
            success:function(){
              callback && callback(2);
            },
            fail:function(){
              callback && callback(1);
            }
          })
        }else
        {
          callback && callback(0);
        }
      }
    };
    this.request(allParams);
  }

  //保存到本地缓存
  execSetStorageSync(data){
    wx.setStorageSync(this._storageKeyName, data)
  }

  //获取历史订单，pageIndex从1开始
  getOrders(pageIndex, callback) {
    var params = {
      url: 'order/by_user',
      data: { page: pageIndex },
      sCallback: function (res) {
        callback && callback(res);
      }
    };
    this.request(params);
  }
  //是否有新的订单
  hasNewOrder(){
    var flag = wx.getStorageInfoSync(this._storageKeyName);
    return flag == true;
  }
}
export {Order};