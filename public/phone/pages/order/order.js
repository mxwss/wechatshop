// order.js
import {Cart} from "../cart/cart-model.js";
import {Order} from "order-model.js";
import {Address} from "../../utils/address.js";
var cart = new Cart();
var order = new Order();
var address = new Address();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var from = options.from;
    if(from == 'cart'){
      this._fromCart(options.account);
    }else
    {
      this._fromOrder(options.id);
    }
    
  },

  _fromCart:function(account){
    var produtsArr = cart.getCartDataFromLocal(true);
    this.data.account = account;
    this.setData({
      productsArr: produtsArr,
      account: account,
      orderStatus: 0
    });
    address.getAddress((res) => {
      this._bindAddressInfo(res);
    });
  },

  _fromOrder:function(id){
    if (id) {
      var that = this;
      //下单后，无论支付成功失败，当点击左上角的返回时都要更新订单状态
      order.getOrderInfoById(id, (res) => {
        that.setData({
          orderStatus: res.status,
          productsArr: res.snap_items,
          account: res.total_price,
          basicInfo: {
            orderTime: res.create_time,
            orderNo: res.order_no
          }
        });
        var addressInfo = res.snap_address;
        addressInfo.totalDetail = address.setAddressInfo(addressInfo);
        that._bindAddressInfo(addressInfo);
      });
    }
  },
  onShow:function(){
    this._fromOrder(this.data.id);
  },

 //用户添加收货地址事件
  editAddress:function(event){
    var that = this;
    wx.chooseAddress({
      success:function(res){
        let detailInfo = {
          name:res.userName,
          mobile:res.telNumber,
          totalDetail: address.setAddressInfo(res)
        };
        that._bindAddressInfo(detailInfo);
        address.submitAddress(res,(flag)=>{
          if(!flag){
            that.showTips('操作提示','地址更新失败');
          }
        });
      }
    });
  },
  /**
   * title:标题
   * content：内容
   * flag：是否跳转到个人中心
   */
  showTips:function(title,content,flag){
    wx.showModal({
      title: title,
      content: content,
      showCancel:false,
      success:function(res){
        if(flag){
          wx.switchTab({
            url: '/pages/my/my',
          })
        }
      }
    })
  },
  //绑定数据 地址
  _bindAddressInfo:function(detailInfo){
    this.setData({
      addressInfo:detailInfo
    });
  },

  //用户支付
  pay:function(event){
    if(!this.data.addressInfo){
      this.showTips('下单提示','请您填写你的售货地址');
      return;
    }
    if(this.data.orderStatus==0){
      this._firstTimePay();
    }else
    {
      this._oneMoreTimePay();
    }
  },

  //第一次支付
  _firstTimePay:function(){
      var orderInfo = [],
          productInfo = this.data.productsArr,
          order = new Order();
      for(let i = 0;i<productInfo.length;i++){
        orderInfo.push({
          product_id:productInfo[i].id,
          count:productInfo[i].counts
        });
      }
      var that = this;
      //支付分两步，第一步是生成订单号，然后根据订单号支付
      order.doOrder(orderInfo,(data)=>{
        //订单生成成功
        if(data.pass){
          var id = data.order_id;
              that.data.id = id;
              that.data.fromCartFlag = false;
              //开始支付
          that._execPay(id);
        }else{
          //订单生成失败
          that._orderFail(data);
        }
      });
  },

  //开始支付
  _execPay:function(id){
    var that = this;
    order.execPay(id,(statusCode)=>{
      if(statusCode!=0){
        //删除已经下单的购物车的商品
        that.deleteProducts();
        var flag = statusCode == 2;
        wx.navigateTo({
          url: '../pay-result/pay-result?id='+id+'&flag='+flag+'&from=order',
        });
      }
    });
  },

  //下单失败
  _orderFail:function(data){
    var nameArr = [],
    name = '',
    str = '',
    pArr = data.pStatusArray;
    for(let i = 0;i<pArr.length;i++){
      if(!pArr[i].haveStock){
        name = pArr[i].name;
        if(name.length>15){
         name = name.substr(0,12) + '...';
        }
        nameArr.push(name);
        if(nameArr.length>=2){
          break;
        }
      }
    }
    str += nameArr.join('、');
    if(nameArr.length>2){
      str += '等';
    }
    str += '缺货';
    wx.showModal({
      title: '下单失败',
      content: str,
      showCancel:false,
      success:function(){
      }
    });
  },

  //删除购物车已经下单的商品
  deleteProducts:function(){
    wx.setStorageSync('cart', '');
  }

})