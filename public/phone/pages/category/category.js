// category.js
import {Category} from 'category-model.js';
var category = new Category();
Page({

  /**
   * 页面的初始数据
   */
  data: {
   loadingHidden:false,
   currentMenuIndex:0,
   transClassArr: ['tanslate0', 'tanslate1', 'tanslate2', 'tanslate3', 'tanslate4', 'tanslate5']
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this._loadData();
  },

  _loadData:function(){
    category.getCategoryType((res)=>{
      this.setData({
        'categoryType':res,
        'loadingHidden':true
      });
      category.getProductsByCategory(res[0].id, (data) => {
        var dataObj = {
          products:data,
          topicImg:res[0].img.url,
          title:res[0].name
        };
        this.setData({
          'categoryInfo0': dataObj,
          'loadingHidden': true
        });
      })
    });
 },

  /*切换分类*/
  changeCategory:function(event){
    var index = category.getDataSet(event,'index');
    var id = category.getDataSet(event,'id');
    this.setData({
      'currentMenuIndex':index
    });
    //如果数据是第一次请求
    if (!this.isLoadedData(index))
    {
      this.getProductsByCategory(id,(data)=>{
        this.setData(
          this.getDataObjForBind(index,data)
        );
      });
    }
  },
  getProductsByCategory: function (id, callback) {
    category.getProductsByCategory(id, (data) => {
      callback && callback(data);
    });
  },
  isLoadedData: function (index) {
    if (this.data['categoryInfo' + index]) {
      return true;
    }
    return false;
  },

  getDataObjForBind:function(index,data){
    var obj = {},
        arr = [0,1,2,3,4,5],
        baseData = this.data.categoryType[index];
        for(var item in arr)
        {
          if(item==arr[index])
          {
            obj['categoryInfo'+index]={
              products:data,
              topicImg:baseData.img.url,
              title:baseData.name
            }
            return obj;
          }
        }
  },
  onProductsItemTap: function (event) {
    var id = category.getDataSet(event, 'id');
    var name = category.getDataSet(event,'name')
    wx.navigateTo({
      url: '../product/product?id=' + id + '&name=' + name,
    });
  },

  
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this._loadData(() => {
      wx.stopPullDownRefresh()
    });
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '小黑小卖部',
      path: 'pages/category/category'
    }
  }
})