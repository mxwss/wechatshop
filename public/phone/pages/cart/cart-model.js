import { Base } from '../../utils/base.js';
class Cart extends Base{
  constructor(){
    super();
    this._storageKeyName = 'cart';
  }


  add(item,counts){
    var cartData = this.getCartDataFromLocal();
    var isHasInfo = this._isHasThatOne(item.id,cartData);
    //判断添加的商品是否在购物车已经存在,等于-1表示不存在
    if(isHasInfo.index == -1)
    {
      item.counts = counts;
      item.selectStatus = true;//设置选中状态
      cartData.push(item);
    }else
    {
      cartData[isHasInfo.index].counts += counts;
    }
    //将购物车商品信息添加到缓存
    wx.setStorageSync(this._storageKeyName,cartData);
  }

  //从缓存取出购物车的数据
  getCartDataFromLocal(flag){
    var res = wx.getStorageSync(this._storageKeyName);
    if(!res)
    {
      res = [];
    }
    //当flag为true时则取出购物车中用户选中的商品，排除掉未选中的商品
    if(flag){
      var newData = [];
      for(let i = 0;i<res.length;i++){
        if(res[i].selectStatus){
          newData.push(res[i]);
        }
      }
      res = newData;
    }
    return res;
  }

  /*获取当前用户的购物车商品数量
  flag为true时考虑选中状态
  */
  getCartTotalCounts(flag){
    var data = this.getCartDataFromLocal();
    var counts = 0;
    for(let i = 0;i<data.length;i++)
    {
      if(flag)
      {
        if(data[i].selectStatus)
        {
          counts += data[i].counts;
        }
      }
      counts += data[i].counts;
    }
    return counts;
  }

/*
判断某个商品是否已经在购物车中
并且返回该商品的数据以及所在数组的序号
 */
  _isHasThatOne(id,arr){
    var item,
        result = {index:-1};
        for(let i = 0;i<arr.length;i++)
        {
          item = arr[i];
          if(item.id==id)
          {
            result = {
              index:i,
              data:item
            };
            break;
          }
        }
        return result;
  }
  //修改商品数目,私有方法
  _changeCounts(id,counts){
    var data = this.getCartDataFromLocal(),
        hasInfo = this._isHasThatOne(id,data);
    if(hasInfo.index != -1){
      if(hasInfo.data.counts >1){
        data[hasInfo.index].counts += counts;
      }
    }
    wx.setStorageSync(this._storageKeyName, data); //更新本地缓存
  }

  //增加商品数量
  addCounts(id){
    this._changeCounts(id,1);
  }
  //减少商品数量
  decreaseCounts(id){
    this._changeCounts(id,-1);
  }

  //删除缓存中的商品,可以传入一个数组，即删除多个商品
  deleteStorageProduct(ids){
    if(!(ids instanceof Array))
    {
      ids = [ids];
    }
    var cartData = this.getCartDataFromLocal();
    for(let i = 0;i<ids.length;i++){
      let hasInfo = this._isHasThatOne(ids[i],cartData);
      if(hasInfo.index != -1){
        cartData.splice(hasInfo.index,1);
      }
    }
    wx.setStorageSync(this._storageKeyName, cartData); //更新本地缓存
  }
}
export {Cart};