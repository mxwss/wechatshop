// cart.js

import { Cart } from 'cart-model.js';
var cart = new Cart();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  //当用户离开当前页时更新购物车状态
   onHide:function(){
     wx.setStorageSync('cart', this.data.cartData);
   },
  /**
   * 生命周期函数--监听页面显示
   * onShow与onLoad的区别是onShow每次点击该页面都会加载，而onLoad只在第一次点击页面加载
   */
  onShow: function () {
    var cartData = cart.getCartDataFromLocal();
    var cal = this._calculateTotalAccount(cartData);
    this.setData({
      selectedCounts: cal.selectedCounts,
      selectedTypeCounts: cal.selectedTypeCounts,
      account: cal.account,
      cartData: cartData
    });
  },

  //计算总金额
  _calculateTotalAccount: function (data) {
    var len = data.length,
      //总金额，要排除未被选中的商品
      account = 0,
      //被选中的商品的总数，因为用户可以买多个同一件商品
      selectedCounts = 0,
      //被选中的商品的数量，一个商品不论用户买了多少都是1个
      selectedTypeCounts = 0;
    //避免0.05+0.01=0.0600的情况，将数量和价格都乘以100
    let multiple = 100;
    for (let i = 0; i < len; i++) {
      if (data[i].selectStatus) {
        account += data[i].counts * multiple * Number(data[i].price) * multiple;
        selectedCounts += data[i].counts;
        selectedTypeCounts++;
      }
    }
    return {
      selectedCounts: selectedCounts,
      selectedTypeCounts: selectedTypeCounts,
      account: account / (multiple * multiple)
    };
  },
  //购物车里商品前面的选择状态事件
  toggleSelect: function (event) {
    var id = cart.getDataSet(event, 'id'),
      status = cart.getDataSet(event, 'status'),
      index = this._getProductIndexById(id);
    this.data.cartData[index].selectStatus = !status;
    this._resetCartData();
  },

  //购物车里全选的选择状态事件
  toggleSelectAll: function (event) {
    var status = cart.getDataSet(event, 'status') == 'true';
    var data = this.data.cartData,
      len = data.length;
    for (let i = 0; i < len; i++) {
      data[i].selectStatus = !status;
    }
    this._resetCartData();
  },

  _resetCartData: function () {
    //重新计算商品总金额
    var newData = this._calculateTotalAccount(this.data.cartData);
    this.setData({
      selectedCounts: newData.selectedCounts,
      selectedTypeCounts: newData.selectedTypeCounts,
      account: newData.account,
      cartData: this.data.cartData
    });
  },
  //根据商品id得到商品所在数组下标
  _getProductIndexById: function (id) {
    var data = this.data.cartData, len = data.length;
    for (let i = 0; i < len; i++) {
      if (data[i].id == id) {
        return i;
      }
    }
  },

  //购物车商品的增减
  changeCounts: function (event) {
    var id = cart.getDataSet(event, 'id'),
      actionType = cart.getDataSet(event, 'type'),
      index = this._getProductIndexById(id),
      counts = 1;
    if (actionType == 'cut') {
      counts = -1;
      cart.decreaseCounts(id);
    } else {
      cart.addCounts(id);
    }
    this.data.cartData[index].counts += counts;
    this._resetCartData();
  },

  //删除商品事件
  deleteProduct:function(event){
    var id = cart.getDataSet(event, 'id'),
      index = this._getProductIndexById(id);
      this.data.cartData.splice(index,1);
      this._resetCartData();
      cart.deleteStorageProduct(id);
  },
  //用户点击购物车的图片也能跳转到商品详情页
  onProductsItemTap: function (event) {
    var id = cart.getDataSet(event, 'id');
    var name = cart.getDataSet(event, 'name')
    wx.navigateTo({
      url: '../product/product?id=' + id + '&name=' + name,
    });
  },

  //用户下单事件
  submitOrder:function(event){
    wx.navigateTo({
      url: '../order/order?account='+ this.data.account +'&from=cart',
    })
  }
})