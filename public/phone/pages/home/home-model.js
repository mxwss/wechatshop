
import {Base} from '../../utils/base.js';
class Home extends Base{
  constructor(){
    super();
  }
  //首页轮播图
  getBannerData(id,callback){
    var params = {
      url:'banner/'+id,
      sCallback:function(res)
      {
        callback && callback(res.banner_items);
      }
    };
    this.request(params);
  }

  //首页主题
  getThemeData(callback){
    var params = {
      url:'theme?ids=1,2,3',
      sCallback:function(res)
      {
        callback && callback(res);
      }
    };
    this.request(params);
  }

  //获取最新商品
  getProductsData(callback) {
    var params = {
      url: 'product/recent',
      sCallback: function (res) {
        callback && callback(res);
      }
    };
    this.request(params);
  }
}
export {Home};