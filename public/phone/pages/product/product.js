// product.js
import {Product} from "product-model.js";
import {Cart} from "../cart/cart-model.js";
var product = new Product();
var cart = new Cart();
Page({

  /**
   * 页面的初始数据
   */
  data: {
  loadingHidden: false,
  hiddenSmallImg: true,
  countsArr:[1,2,3,4,5,6,7,8,9,10],
  productCount:1,
  productTabBar:['商品详情','产品参数','售后保障'],
  currentTapIndex:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.data.name = options.name;
    this.data.id = options.id;
    this._loadData();
  },

  _loadData:function(){
  product.getDetailInfo(this.data.id,(res)=>{
    this.setData({
      'cartTotalCounts': cart.getCartTotalCounts(),
      'product':res
    });
  });
  },

  /*获取用户的下单商品数量*/
  bindPickerChange:function(event){
    var index = event.detail.value;
    this.setData({
      'productCount':this.data.countsArr[index]
    });
  },

  onTabsItemTap:function(event){
    var index = product.getDataSet(event,'index');
    this.setData({
      'currentTapIndex':index
    });
  },

 /*添加商品事件*/
  onAddingToCartTap:function(event){
    this.addToCart();
    var counts = this.data.productCount + this.data.cartTotalCounts;
    this.setData({
      'cartTotalCounts': counts
    });
  },

 /*添加商品到购物车*/
  addToCart:function(){
    var tempObj = {};
    var keys = ['id','name','main_img_url','price'];
    for(var key in this.data.product)
    {
      if(keys.indexOf(key)>=0)
      {
        tempObj[key] = this.data.product[key];
      }
    }
    cart.add(tempObj,this.data.productCount);
  },
  /**
   * 让标题栏的名字变成商品名字
   */
  onReady: function () {
    wx.setNavigationBarTitle({
      title: this.data.name,
    });
  },

  onCartTap:function(event){
    wx.switchTab({
      url: '/pages/cart/cart',
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this._loadData(() => {
      wx.stopPullDownRefresh()
    });
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '小黑小卖部',
      path: 'pages/product/product?id=' + this.data.id
    }
  }
})