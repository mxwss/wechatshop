// theme.js
import { Theme } from 'theme-model.js';
var theme = new Theme();
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   this.data.id = options.id;
   this.data.name = options.name;
   this._loadData();
  },

  //动态改变标题栏题目
  onReady:function(){
    wx.setNavigationBarTitle({
      title: this.data.name,
    });
  },
  onProductsItemTap: function (event) {
    var id = theme.getDataSet(event, 'id');
    var name = theme.getDataSet(event, 'name');
    wx.navigateTo({
      url: '../product/product?id=' + id + '&name=' + name,
    });
  },
  _loadData:function()
  {
      theme.getProductsData(this.data.id,(res)=>{
        this.setData({
          'themeInfo':res
        });
      });
  },

  onShareAppMessage:function(){
    return {
      title: '小黑小卖部',
      path: 'pages/theme/theme?id=' + this.data.id
    };
  }
})