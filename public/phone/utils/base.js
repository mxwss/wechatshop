import {Token} from "token.js";
import {Config} from '../utils/config.js';
class Base{
  constructor(){
    this.baseRequestUrl = Config.restUrl;
  }
  //自己封装的向服务器发送请求的方法
  request(params,noRefetch){
    var that = this;
    var url = this.baseRequestUrl + params.url;
    if(!params.type)
    {
      params.type = 'GET';
    }
    wx.request({
      url: url,
      data: params.data,
      header: {
        'content-type':'application/json',
        'token':wx.getStorageSync('token')
      },
      method: params.type,
      success: function (res) { 
        //下面是当sCallBack存在时才执行函数sCallBack
        var code = res.statusCode.toString();
        var startChar = code.charAt(0);
        if(startChar == '2'){
          params.sCallback && params.sCallback(res.data);
        }else
        {
          if(code == '401')
          {
            if(!noRefetch){
              that._refetch(params);
            }
          }
          if(noRefetch){
            params.eCallback && params.eCallback(res.data);
          }
        }
      },
      fail: function (err) {
        console.log(err);
       },
    })
  }
  //当http状态吗是401的时候从新向服务器请求获取token，同时将再次发起业务请求request
  _refetch(params){
    var token = new Token();
    token.getTokenFromServer((res)=>{
        this.request(params,true);
    });
  }
  //获得元素上绑定的值
  getDataSet(event,key)
  {
    return event.currentTarget.dataset[key];
  }
}
export {Base};