import {Base} from 'base.js';
import {Config} from 'config.js';

class Address extends Base{
  constructor(){
    super();
  }
  //设置用户地址
  setAddressInfo(res){
    let province = res.provinceName || res.province,
        city = res.cityName || res.city,
        country = res.countyName || res.country,
        detail = res.detailInfo || res.detail;
    let address = city + country + detail;
    if(!this.isCenterCity(province))
    {
      address  = province + address;
    }
    return address;
  }
  //获取用户地址
  getAddress(callback){
    var that = this;
    let params = {
      url:'getAddress',
      sCallback:function(res){
        if(res){
          res.totalDetail = that.setAddressInfo(res);
          callback && callback(res);
        }
      }
    };
    this.request(params);
  }
  //判断直辖市
  isCenterCity(name){
    let cities = ['北京市','天津市','上海市','重庆市'],
        flag = cities.indexOf(name) >= 0;
    return flag;
  }

  //更新保存地址
  submitAddress(data,callback){
    data = this._setUpAddress(data);
    let params = {
      url:'address',
      type:'POST',
      data:data,
      sCallback:function(res){
        callback&&callback(true,res);
      },eCallback:function(res){
        callback&&callback(false,res);
      }
    };
    this.request(params);
  }

  //保存地址
  _setUpAddress(res){
    let data = {
      name:res.userName,
      country:res.countyName,
      city:res.cityName,
      province:res.provinceName,
      mobile:res.telNumber,
      detail:res.detailInfo
    };
    return data;
  }
}
export {Address};