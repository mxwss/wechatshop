<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\Route;

Route::get('api/:version/banner/:id','api/:version.Banner/getBanner'); //首页轮播图

Route::get('api/:version/theme','api/:version.theme/getSimpleList');  //商品专题
Route::get('api/:version/theme/:id','api/:version.theme/getComplexOne');  //专题详情

Route::get('api/:version/product/recent','api/:version.product/recent');   //最新商品
Route::get('api/:version/product/category/:id','api/:version.product/getAllInCategory');  //单个分类商品
Route::get('api/:version/product/:id','api/:version.product/getOne',[],['id'=>'\d+']);   //单个商品


Route::get('api/:version/category/all','api/:version.category/getAllCategories');   //所有商品分类

Route::post('api/:version/token/user','api/:version.token/getToken');   //获取token
Route::post('api/:version/token/verify','api/:version.token/verifyToken');   //验证token
Route::post('api/:version/token/app','api/:version.token/getAppToken');   //第三方app获取token

Route::post('api/:version/address','api/:version.address/createOrUpdateAddress');   //创建用户地址
Route::get('api/:version/getAddress','api/:version.address/getUserAddress');   //创建用户地址

Route::post('api/:version/order','api/:version.order/placeOrder');   //用户下单
Route::get('api/:version/order/:id','api/:version.order/getDetail',[],['id'=>'\d+']);   //历史订单详情
Route::get('api/:version/order/by_user','api/:version.order/getSummaryByUser');   //获取历史订单
Route::get('api/:version/order/paginate','api/:version.order/getSummary');   //cms获取所有订单列表
Route::put('api/:version/order/delivery','api/:version.order/delivery');   //发送模板消息

Route::post('api/:version/pay/pre_order','api/:version.pay/getPreOrder');   //获取微信预订单
Route::post('api/:version/pay/notify','api/:version.pay/receiveNotify');   //微信回调地址
Route::post('api/:version/pay/re_notify','api/:version.pay/redirectNotify');   //微信回调处理

Route::get('admin/index','admin/index/index');