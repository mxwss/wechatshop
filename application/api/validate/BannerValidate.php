<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/27 0027
 * Time: 上午 10:15
 */

namespace app\api\validate;




class BannerValidate extends BaseValidate
{
    protected $rule = [
        'name'=>'require|max:20'
    ];
    protected $message = [
        'name.required'=>'姓名不能为空',
        'name.max'=>'姓名最大长度为20个字符'
    ];

}