<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/28 0028
 * Time: 下午 13:39
 */

namespace app\api\validate;


class CountValidate extends BaseValidate
{
    protected $rule = [
        'count'=>'integer|between:1,15'
    ];
    protected $message = [
        'count.integer'=>'count必须是整数',
        'count.between'=>'count必须在范围1-15之间'
    ];
}