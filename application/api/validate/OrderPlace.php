<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/5 0005
 * Time: 上午 9:37
 */

namespace app\api\validate;


use app\api\exception\BaseException;

class OrderPlace extends BaseValidate
{
    //下单商品参数验证大
    protected $rule = [
        'products'=>'array|checkProducts'
    ];

    //下单单个商品参数验证
    protected $singleRule = [
        'product_id'=>'require|integer',
        'count'=>'require|integer'
    ];
    protected function checkProducts($value)
    {
        if(empty($value))
        {
            throw new BaseException('商品列表不能为空');
        }
        foreach ($value as $k => $v)
        {
            $this->checkProduct($v);
        }
        return true;
    }

    //检测商品参数
    protected function checkProduct($value)
    {
        $validate = new BaseValidate($this->singleRule);
        $result = $validate->check($value);
        if(!$result)
        {
            throw new BaseException('商品列表参数错误');
        }
    }
}