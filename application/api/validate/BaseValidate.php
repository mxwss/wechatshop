<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/27 0027
 * Time: 上午 10:59
 */

namespace app\api\validate;



use think\Validate;

class BaseValidate extends Validate
{
    public function goCheck()
    {
        $data = input('param.');
        $result = $this->batch()->check($data);
        if(!$result)
        {
            $error = $this->error;
            if (is_array($error))
            {
                $error = implode(';',$error);
            }
            exception($error,0);
        }else
        {
            return true;
        }
    }

    protected function isNotEmpty($value, $rule='', $data='', $field='')
    {
        if (empty($value)) {
            return $field . '不允许为空';
        } else {
            return true;
        }
    }
    //检测是否是手机
    protected function isMobile($value)
    {
        $rule = '^1(3|4|5|7|8)[0-9]\d{8}$^';
        $result = preg_match($rule, $value);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function getDataByRule($arrays)
    {
        if(array_key_exists('user_id',$arrays)||array_key_exists('uid',$arrays))
        {
            exception('非法参数user_id或者uid');
        }
        $newArray = [];
        foreach ($this->rule as $k => $value)
        {
            $newArray[$k] = $arrays[$k];
        }
        return $newArray;
    }
}