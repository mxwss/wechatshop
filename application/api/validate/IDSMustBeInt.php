<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/28 0028
 * Time: 上午 10:04
 */

namespace app\api\validate;


class IDSMustBeInt extends BaseValidate
{
    protected $rule = [
        'ids'=>'require|checkIDS'
    ];
    protected $message = [
        'ids.integer'=>'ids必须是以逗号分割的整数',
        'ids.require'=>'ids参数必须填',
        'ids.checkIDS'=>'ids必须是以逗号分割的整数'
    ];

    //使用自定义规则来验证数据
    protected function checkIDS($value)
    {
        if(strlen($value)==1)
        {
            if(!is_numeric($value) && !is_int($value))
            {
                return false;
            }
            return true;
        }
        $data = explode(',',$value);
        if(empty($data))
        {
            return false;
        }
        foreach ($data as $v)
        {
            if(!is_numeric($v) && !is_int($v))
            {
                return false;
            }
        }
        return true;
    }
}