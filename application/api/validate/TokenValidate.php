<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/28 0028
 * Time: 下午 15:33
 */

namespace app\api\validate;


class TokenValidate extends BaseValidate
{
    protected $rule = [
        'code'=>'require|/\w+/i'
    ];
    protected $message = [
        'code.require'=>'code参数缺失',
        'code./\w+/i'=>'code不能为空'
    ];
}