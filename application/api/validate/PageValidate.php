<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/14 0014
 * Time: 上午 9:59
 */

namespace app\api\validate;


class PageValidate extends BaseValidate
{
    protected $rule = [
        'page'=>'integer',
        'size'=>'integer'
    ];
    protected $message = [
        'page.integer'=>'page参数必须是正整数',
        'size.integer'=>'size参数必须是正整数',
    ];
}