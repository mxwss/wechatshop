<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/23 0023
 * Time: 上午 7:56
 */

namespace app\api\validate;


class AppTokenGet extends BaseValidate
{
    protected $rule = [
        'ac'=>'require|isNotEmpty',
        'se'=>'require|isNotEmpty'
    ];
}