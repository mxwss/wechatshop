<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/27 0027
 * Time: 上午 10:32
 */

namespace app\api\validate;


use think\Validate;

class IDMustInt extends BaseValidate
{
    protected $rule = [
        'id'=>'integer'
    ];
    protected $message = [
        'id.integer'=>'id必须是整数'
    ];
}