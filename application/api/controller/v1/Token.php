<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/28 0028
 * Time: 下午 15:32
 */

namespace app\api\controller\v1;


use app\api\exception\BaseException;
use app\api\service\AppToken;
use app\api\service\UserToken;
use app\api\validate\AppTokenGet;
use app\api\validate\TokenValidate;
use app\api\service\Token as TokenService;
class Token
{
    //用户获取token
    public function getToken($code='')
    {
        (new TokenValidate())->goCheck();
        $ut = new UserToken($code);
        $token = $ut->get();
        return json(['token'=>$token]);
    }
    
    //验证token的合法性
    public function verifyToken($token='')
    {
        if(!$token){
            throw new BaseException("token不能为空");
        }
        $validate = TokenService::verifyToken($token);
        return json(['isValid'=>$validate]);
    }
    
    //第三方应用获取token
    public function getAppToken($ac='',$se='')
    {
        (new AppTokenGet())->goCheck();
        $app = new AppToken();
        $token = $app->get($ac,$se);
        return json(['token'=>$token]);
    }
}