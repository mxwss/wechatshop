<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/29 0029
 * Time: 下午 16:12
 */

namespace app\api\controller\v1;


use app\api\controller\BaseController;
use app\api\exception\SuccessException;
use app\api\exception\UserException;
use app\api\model\User;
use app\api\model\UserAddress;
use app\api\service\Token as TokenService;
use app\api\validate\AddressNew;
class Address extends BaseController
{
    protected $beforeActionList = [
        'checkPrimaryScope'=>['only'=>'createOrUpdateAddress,getUserAddress']
    ];

    //将用户的送货地址存入数据库
    public function createOrUpdateAddress()
    {
        $validate = new AddressNew();
        $validate->goCheck();
        //获取当前用户uid
        $uid = TokenService::getCurrentUid();
        $user = User::get($uid);
        if (!$user)
        {
            throw new UserException();
        }
        $dataArray = $validate->getDataByRule(input('post.'));
        $userAddress = $user->address;
        if(!$userAddress)
        {
            $user->address()->save($dataArray);
        }else
        {
            $user->address->save($dataArray);
        }
        return json(new SuccessException(),201);
    }

    //获取用户的地址
    public function getUserAddress()
    {
        $uid = TokenService::getCurrentUid();
        $address = UserAddress::where('id',$uid)->find();
        if(!$address){
            throw new UserException("用户地址不存在",60001);
        }
        return $address;
    }
}