<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/28 0028
 * Time: 上午 9:35
 */

namespace app\api\controller\v1;


use app\api\exception\ThemeException;
use app\api\validate\IDMustInt;
use app\api\validate\IDSMustBeInt;
use app\api\model\Theme as ThemeModel;
use think\composer\ThinkExtend;

class Theme
{
    /**
     * 首页专题栏目
     * @url /theme?ids=id1,id2,id3....
     * @param string $ids
     * @return \think\response\Json
     * @throws ThemeException
     */
    public function getSimpleList($ids='')
    {
        (new IDSMustBeInt())->goCheck();
        $id = explode(',',$ids);
        $data = ThemeModel::getTheme($id);
        if($data->isEmpty())
        {
            throw new ThemeException();
        }
        return json($data);
    }

    /**
     * 单个专题栏目详情
     * @url /theme/id
     * @param int $id
     * @return \think\response\Json
     * @throws ThemeException
     */
    public function getComplexOne($id)
    {
        (new IDMustInt())->goCheck();
        $data = ThemeModel::getThemeData($id);
        if(!$data)
        {
            throw new ThemeException();
        }
        return json($data);
    }
}