<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/28 0028
 * Time: 下午 13:36
 */

namespace app\api\controller\v1;


use app\api\exception\ProductCount;
use app\api\exception\ProductDetail;
use app\api\exception\ProductInCategory;
use app\api\validate\CountValidate;
use app\api\model\Product as ProductModel;
use app\api\validate\IDMustInt;

class Product
{
    /**
     * @url /product/recent?count=显示数量
     * @param int $count
     * @return \think\response\Json
     * @throws ProductCount
     */
    public function recent($count=15)
    {
        (new CountValidate())->goCheck();
        $data = ProductModel::getRecent($count);
        if($data->isEmpty())
        {
            throw new ProductCount();
        }
        $data->hidden(['summary']);
        return json($data);
    }

    /**
     * @url /product/category/id
     * @param int $id
     * @return \think\response\Json
     * @throws ProductInCategory
     */
    public function getAllInCategory($id)
    {
        (new IDMustInt())->goCheck();
        $data = ProductModel::getProductInCategory($id);
        if($data->isEmpty())
        {
            throw new ProductInCategory();
        }
        $data->hidden(['summary','category_id']);
        return json($data);
    }

    public function getOne($id)
    {
        (new IDMustInt())->goCheck();
        $data = ProductModel::getProductDetail($id);
        if(!$data)
        {
            throw new ProductDetail();
        }
        return json($data);
    }
}