<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/11 0011
 * Time: 上午 9:58
 */

namespace app\api\controller\v1;


use app\api\controller\BaseController;
use app\api\service\WxNotify;
use app\api\validate\IDMustInt;
use app\api\service\Pay as PayService;
class Pay extends BaseController
{
    protected $beforeActionList = [
        'checkExclusiveScope'=>['only'=>'getPreOrder']
    ];

    public function getPreOrder($id='')
    {
        (new IDMustInt())->goCheck();
        $pay = new PayService($id);
        return $pay->pay();
    }

    //微信回调处理
    public function redirectNotify()
    {
        $notify = new WxNotify();
        $notify->Handle();
    }

    //通过自定义函数来调试微信的回调结果
    public function receiveNotify()
    {
        $data = file_get_contents('php://input');
        $result = curl_post_raw('http://wechatshop.com/v1/pay/re_notify?XDEBUG_SESSION_START=12798',$data);
    }
}