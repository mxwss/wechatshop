<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/3 0003
 * Time: 下午 16:54
 */

namespace app\api\controller\v1;


use app\api\controller\BaseController;
use app\api\exception\OrderException;
use app\api\exception\SuccessException;
use app\api\validate\IDMustInt;
use app\api\validate\OrderPlace;
use app\api\service\Token as TokenService;
use app\api\service\Order as OrderService;
use app\api\validate\PageValidate;
use app\api\model\Order as OrderModel;
class Order extends BaseController
{
    protected $beforeActionList = [
        'checkExclusiveScope'=>['only'=>'placeOrder'],
        'checkPrimaryScope'=>['only'=>'getSummaryByUser,getDetail'],
    ];

    //获取历史订单
    public function getSummaryByUser($page=1,$size=15)
    {
        (new PageValidate())->goCheck();
        $uid = TokenService::getCurrentUid();
        $pageData = OrderModel::getSummaryByUser($uid,$page,$size);
        //因为返回的是paginate对象，所以不能直接判断对象是否为空，要调用对象的方法
        if($pageData->isEmpty())
        {
            return [
                'data'=>[],
                'current_page'=>$pageData->getCurrentPage()
            ];
        }
        $data = $pageData->hidden(['snap_items','snap_address','prepay_id'])->toArray();
        return [
            'data'=>$data,
            'current_page'=>$pageData->getCurrentPage()
        ];
    }
    //订单详情
    public function getDetail($id)
    {
        (new IDMustInt())->goCheck();
        $order = OrderModel::get($id);
        if(!$order)
        {
            throw new OrderException();
        }
        return $order->hidden(['prepay_id']);
    }
    //用户下单
    public function placeOrder()
    {
        (new OrderPlace())->goCheck();
        $data = input('products/a');
        $uid = TokenService::getCurrentUid();
        $order = new OrderService();
        $status = $order->place($uid,$data);
        return $status;
    }

    //cms获取订单列表
    public function getSummary($page=1,$size=20)
    {
        (new PageValidate())->goCheck();
        $pageOrders = OrderModel::getSummaryByPage($page,$size);
        if($pageOrders->isEmpty()){
            return json(['current_page'=>$pageOrders->currentPage(),'data'=>[]]);
        }
        $data = $pageOrders->hidden(['snap_items','snap_address'])->toArray();
        return json(['current_page'=>$pageOrders->currentPage(),'data'=>$data]);
    }

    //向小程序端发送模板消息
    public function delivery($id)
    {
        (new IDMustInt())->goCheck();
        $order = new OrderService();
        $success = $order->delivery($id);
        if($success){
            return new SuccessException();
        }
    }
}