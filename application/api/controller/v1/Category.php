<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/28 0028
 * Time: 下午 14:21
 */

namespace app\api\controller\v1;

use app\api\model\Category as CategoryModel;
use app\api\exception\CategoryException;
class Category
{
    /**
     * @url /category/all
     * @return \think\response\Json
     * @throws CategoryException
     */
    public function getAllCategories()
    {
        $data = CategoryModel::getCategories();
        if($data->isEmpty())
        {
            throw new CategoryException();
        }
        return json($data);
    }
}