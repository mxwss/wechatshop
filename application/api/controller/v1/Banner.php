<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/27 0027
 * Time: 上午 9:13
 */

namespace app\api\controller\v1;

use app\api\exception\BannerException;
use app\api\model\Banner as BannerModel;
use app\api\validate\IDMustInt;
class Banner
{
    /**
     * @url /banner/:id
     * @http GET
     * @id banner的id
     */
    public function getBanner($id)
    {
        (new IDMustInt())->goCheck();
        $banner = BannerModel::getBannerID($id);
        if(!$banner)
        {
            throw new BannerException();
        }
        return json($banner);
    }
}