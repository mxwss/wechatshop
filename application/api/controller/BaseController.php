<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/5 0005
 * Time: 上午 9:15
 */

namespace app\api\controller;

use think\Controller;
use app\api\service\Token as TokenService;
class BaseController extends Controller
{
    //用户和管理员都能访问接口的权限检查
    public function checkPrimaryScope()
    {
        TokenService::needPrimaryScope();
    }
    //只有用户才能访问的接口权限检查
    public function checkExclusiveScope()
    {
        TokenService::needExclusiveScope();
    }
}