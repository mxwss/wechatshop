<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/5 0005
 * Time: 上午 10:24
 */

namespace app\api\service;


use app\api\enum\OrderStatusEnum;
use app\api\exception\OrderException;
use app\api\exception\UserException;
use app\api\model\OrderProduct;
use app\api\model\Product;
use app\api\model\UserAddress;
use app\api\model\Order as OrderModel;
use app\api\validate\PageValidate;
use think\Db;
use think\Exception;
class Order
{
    /*用户选择商品后，向api提交包含它选择商品的相关信息
    api在接收到信息后，需要检查订单相关商品的库存量
    有库存，把订单数据存入数据库中，下单成功，返回客户端消息，告诉客户可以支付了
    调用支付接口，进行支付
    还需要再次进行库存量检查，因为当用户在支付的时候，有可能库存已经没有了
    当库存还有，服务器端就可以调用微信的支付接口进行支付
    小程序端根据服务器返回结果拉起微信支付的界面
    微信会返回给我们一个支付的结果（异步）
    成功，也需要对库存量进行检查
    库存充足，进行库存量的扣除
    */
    //订单商品信息
    protected $oProducts;
    //数据库商品信息
    protected $products;
    //用户id
    protected $uid;

    public function place($uid,$oProducts)
    {
        //对比接收的products商品信息和数据库查询的真实商品信息
        $this->oProducts = $oProducts;
        $this->uid = $uid;
        $this->products = $this->getProductsByOrder($oProducts);
        $status = $this->getOrderStatus();
        if(!$status['pass'])
        {
            $status['order_id'] = -1;
            return $status;
        }
        //库存量充足则创建订单
        $snapOrder = $this->orderSnap($status);
        $order = $this->createOrder($snapOrder);
        $order['pass'] = true;
        return $order;
    }

    //创建订单,额外使用了数据库的事务，防止一个成功另外一个不成功的情况
    private function createOrder($snap)
    {
        Db::startTrans();
        try{
            $data = [
                'order_no'=>self::makeOrderNo(),
                'user_id'=>$this->uid,
                'total_price'=>$snap['orderPrice'],
                'total_count'=>$snap['totalCount'],
                'snap_img'=>$snap['snapImg'],
                'snap_name'=>$snap['snapName'],
                'snap_address'=>$snap['snapAddress'],
                'snap_items'=>json_encode($snap['pStatus'],JSON_UNESCAPED_UNICODE),
            ];
            $order = OrderModel::create($data);
            $orderID = $order->id;
            $create_time = $order->create_time;
            foreach ($this->oProducts as &$p)
            {
                $p['order_id'] = $orderID;
            }
            $orderProduct = new OrderProduct();
            $orderProduct->saveAll($this->oProducts);
            Db::commit();
            return [
                'order_no'=>$data['order_no'],
                'order_id'=>$orderID,
                'create_time'=>$create_time
            ];
        }catch (Exception $e)
        {
            Db::rollback();
            throw $e;
        }
    }
    //生成一个唯一的订单号
    public static function makeOrderNo()
    {
        $code = ['A','B','C','D','E','F','G','H','I'];
        $orderNo = $code[intval(date('Y')-2017)].strtoupper(dechex(date('m'))).date('d').substr(time(),-5).substr(microtime(),2,5).sprintf("%02d",mt_rand(0,99));
        return $orderNo;
    }
    //订单快照，存储用户下单的时刻的商品信息，无论以后商品是否变化，这里的数据都是不变的
    private function orderSnap($status)
    {
        $snap = [
            'orderPrice'=>0,
            'totalCount'=>0,
            'pStatus' => [],
            'snapAddress'=> null,
            'snapImg' => '',
            'snapName'=>''
        ];
        $snap['orderPrice']=$status['orderPrice'];
        $snap['totalCount']=$status['totalCount'];
        $snap['pStatus']=$status['pStatusArray'];
        $snap['snapAddress']=json_encode($this->getUserAddress(),JSON_UNESCAPED_UNICODE);
        $snap['snapImg']=$this->products[0]['main_img_url'];
        $snap['snapName']=$this->products[0]['name'];
        if(count($this->products)>1)
        {
            $snap['snapName'] .= '等';
        }
        return $snap;
    }

    //获取用户的地址
    private function getUserAddress()
    {
        $userAddress = UserAddress::where('user_id',$this->uid)->find();
        if(!$userAddress)
        {
            throw new UserException("用户收货地址不存在,下单失败",60001);
        }
        return $userAddress->toArray();
    }
    //根据订单商品信息查找真实商品信息
    /*
     * 订单传入的参数像这样
     * protected $oProducts = [
     *      [
     *       'product_id'=>1,
     *      'count'=>2
     *      ],
     *      [
     *      'product_id'=>2,
     *      'count'=>3
     *      ],
     *              ];
     */
    private function getProductsByOrder($oProducts)
    {
        $oIds = [];
        foreach ($oProducts as $v)
        {
            array_push($oIds,$v['product_id']);
        }
        $products = Product::all($oIds)
            ->visible(['id','price','stock','name','main_img_url'])
            ->toArray();
        return $products;
    }

    //检测订单的状态，这是购物车的所有商品总计
    private function getOrderStatus()
    {
        $status = [
            'pass'=>true,
            'orderPrice'=>0,
            'totalCount'=>0,
            'pStatusArray'=>[]
        ];
        foreach ($this->oProducts as $oProduct)
        {
            $pStatus = $this->getProductsStatus($oProduct['product_id'],$oProduct['count'],$this->products);
            if(!$pStatus['haveStock'])
            {
                $status['pass'] = false;
            }
            $status['orderPrice'] += $pStatus['totalPrice'];
            $status['totalCount'] += $pStatus['counts'];
            array_push($status['pStatusArray'],$pStatus);
        }
        return $status;
    }

    //对单个商品检测并计算价格，即购物车每一个商品检测
    private function getProductsStatus($oPID,$oCount,$products)
    {
        $pIndex = -1;
        $pStatus = [
            'id'=>null,
            'haveStock'=>false,
            'counts'=>0,
            'name'=>'',
            'totalPrice'=>0,
            'price'=>0,
            'main_img_url'=>null
        ];
        for ($i = 0;$i<count($products);$i++)
        {
            if($oPID == $products[$i]['id'])
            {
                $pIndex = $i;
            }
        }
        if($pIndex == -1)
        {
            throw new OrderException('id为:'.$oPID.'的商品不存在，创建订单失败');
        }else
        {
            $product = $products[$pIndex];
            $pStatus['id'] = $product['id'];
            $pStatus['name'] = $product['name'];
            $pStatus['counts'] = $oCount;
            $pStatus['price'] = $product['price'];
            $pStatus['main_img_url'] = $product['main_img_url'];
            $pStatus['totalPrice'] = $oCount*$product['price'];
            if ($product['stock'] - $oCount >= 0)
            {
                $pStatus['haveStock']= true;
            }
            return $pStatus;
        }
    }
    
    //支付时检测库存量
    public function checkOrderStock($orderID)
    {
        $oProduct = OrderProduct::where('order_id',$orderID)->select();
        $this->oProducts = $oProduct;
        $this->products = $this->getProductsByOrder($oProduct);
        $status = $this->getOrderStatus();
        return $status;
    }

    //发送模板消息
    public function delivery($orderID,$jumpPage='')
    {
        $order = OrderModel::where('id',$orderID)->find();
        if(!$order){
            throw new OrderException();
        }
        if($order->status != OrderStatusEnum::PAID){
            throw new OrderException("订单未支付",80002);
        }
        $order->status = OrderStatusEnum::PAID;
        $order->save();
        $message = new DeliveryMessage();
        return $message->sendDeliveryMessage($order,$jumpPage);
    }

}