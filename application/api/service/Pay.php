<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/11 0011
 * Time: 上午 10:13
 */

namespace app\api\service;

use app\api\enum\OrderStatusEnum;
use app\api\exception\OrderException;
use app\api\exception\TokenException;
use app\api\service\Order as OrderService;
use app\api\model\Order as OrderModel;
use think\Loader;
use think\Log;

Loader::import('WxPay.WxPay',EXTEND_PATH,'.Api.php');
class Pay
{
    private $orderID;
    private $orderNO;
    function __construct($orderID)
    {
        if(!$orderID)
        {
            exception("orderID不能为空");
        }
        $this->orderID = $orderID;
    }
    /*
    * 可能忽略的地方
    * 1.订单根本不存在
    * 2.一个用户去操作别人的订单
    * 3.订单已经被支付了
     * 4.进行库存量检查
    */
    public function pay()
    {
        //检查上面的1,2,3
        $this->checkOrderValidate();
        $order = new OrderService();
        //检查上面的4
        $status = $order->checkOrderStock($this->orderID);
        if(!$status['pass'])
        {
            return $status;
        }
        return $this->makeWxPreOrder($status['orderPrice']);
    }

    //像微信服务器发送请求获取预订单信息
    private function makeWxPreOrder($totalPrice)
    {
        $openid = Token::getCurrentTokenVar('openid');
        if(!$openid)
        {
            throw new TokenException();
        }
        $wxOrderData = new \WxPayUnifiedOrder();
        $wxOrderData->SetOut_trade_no($this->orderNO);
        $wxOrderData->SetTrade_type('JSAPI');
        $wxOrderData->SetTotal_fee($totalPrice*100);
        $wxOrderData->SetBody('小黑小卖部');
        $wxOrderData->SetOpenid($openid);
        $wxOrderData->SetNotify_url(config('setting.pay_back_url'));
        return $this->getPaySignature($wxOrderData);
    }

    //获取预订单信息
    private function getPaySignature($wxOrderData)
    {
        $result = \WxPayApi::unifiedOrder($wxOrderData);
        if($result['return_code'] != 'SUCCESS'|| $result['result_code'] != 'SUCCESS')
        {
            Log::record($result,'error');
            Log::record('获取预支付订单失败','error');
        }
        $this->recordPreOrder($result['prepay_id']);
        return $this->sign($result);
    }

    //调用微信sdk实现参数的组合和签名的生成，这里最后生成一组返回到小程序端的参数用于调用支付接口
    private function sign($result)
    {
        $wxJsPay = new \WxPayJsApiPay();
        $wxJsPay->SetAppid(config('setting.app_id'));
        $wxJsPay->SetTimeStamp((string)time());
        $str = md5(time().mt_rand(0,9999));
        $wxJsPay->SetNonceStr($str);
        $wxJsPay->SetPackage('prepay_id='.$result['prepay_id']);
        $wxJsPay->SetSignType('md5');
        $sign = $wxJsPay->MakeSign();
        $originData = $wxJsPay->GetValues();
        $originData['paySign'] = $sign;
        unset($originData['appId']);
        return $originData;
    }
    //将从微信服务器的预订单信息中的prepay_id存入数据库，服务器可以用这个向客户端主动推送支付成功时的内容
    private function recordPreOrder($prepay_id)
    {
        OrderModel::where('id',$this->orderID)->update(['prepay_id'=>$prepay_id]);
    }

    //对订单进行检测
    private function checkOrderValidate()
    {
        $order = OrderModel::where('id',$this->orderID)->find();
        if(!$order)
        {
            throw new OrderException();
        }
        if(!Token::isValidateOperate($order->user_id))
        {
            throw new TokenException("订单与用户不匹配",10003);
        }
        if($order->status != OrderStatusEnum::UNPAID)
        {
            throw new OrderException("订单已支付了",80003);
        }
        $this->orderNO = $order->order_no;
        return true;
    }
}