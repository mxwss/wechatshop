<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/28 0028
 * Time: 下午 15:51
 */

namespace app\api\service;


use app\api\exception\TokenException;
use app\api\exception\WxExcepton;
use app\api\model\User as UserModel;
class UserToken extends Token
{
    protected $wxAppID;
    protected $wxAppSecret;
    protected $code;
    protected $wxLoginUrl;
    function __construct($code)
    {
        $this->code = $code;
        $this->wxAppID = config('wx.app_id');
        $this->wxAppSecret = config('wx.app_secret');
        $this->wxLoginUrl = sprintf(config('wx.login_url'),$this->wxAppID,$this->wxAppSecret,$this->code);
    }

    //通过curl连接微信服务器得到openID
    public function get()
    {
        $result = json_decode(curl_get($this->wxLoginUrl),true);
        if(empty($result))
        {
            exception('获取openID失败',400);
        }else
        {
            $loginFail = array_key_exists('errcode',$result);
            if($loginFail)
            {
                $this->processLoginException($result);
            }else
            {
               return $this->grantToken($result);
            }
        }
    }


    //微信连接成功后获得openID,生成令牌token
    private function grantToken($result)
    {
        $openid = $result['openid'];
        $user = UserModel::selectUserByToken($openid);
        if ($user)
        {
            $uid = $user->id;
        } else
        {
            $uid = $this->newUser($openid);
        }
        $wxResult = $this->prepareCacheValue($result,$uid);
        $token = $this->saveToCache($wxResult);
        return $token;
    }

    //将微信连接成功的结果接收，做存入缓存前的准备
    private function prepareCacheValue($wxResult,$uid)
    {
        $result = $wxResult;
        $result['uid'] = $uid;    //用户唯一标识
        $result['scope'] = 16;    //用户权限
        return $result;
    }

    //实现存入缓存
    private function saveToCache($wxResult)
    {
        $key = self::generateToken();
        $value = json_encode($wxResult);
        $expire_in = config('setting.token_expire_in');
        $cache = cache($key,$value,$expire_in);
        if(!$cache)
        {
            throw new TokenException('服务器缓存异常',10005);
        }
        return $key;
    }
    //当数据库不存在该用户时，存入该用户的信息
    private function newUser($openid)
    {
        $user = UserModel::create([
            'openid'=>$openid
        ]);
        return $user->id;
    }

    //微信连接失败的错误信息
    private function processLoginException($result)
    {
        throw new WxExcepton($result['errmsg'],$result['errcode']);
    }
}