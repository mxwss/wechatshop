<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/23 0023
 * Time: 下午 14:01
 */

namespace app\api\service;


use app\api\exception\OrderException;
use app\api\exception\UserException;
use app\api\model\User;

class DeliveryMessage extends WxMessage
{
    const DELIVERY_MESSAGE_ID = 1; //模板消息id

    public function sendDeliveryMessage($order,$tplJumpPage='')
    {
        if(!$order){
            throw new OrderException();
        }
        $this->tplID = self::DELIVERY_MESSAGE_ID;
        $this->fromID = $order->prepay_id;
        $this->page = $tplJumpPage;
        $this->prepareMessageData($order);
        $this->emphasisKeyWord = 'keyWord2.DATA';
        return parent::sendMessage($this->getUserOpenID($order->user_id));
    }

    private function prepareMessageData($order)
    {
        $dt = new \DateTime();
        $data = [
            'keyword1'=>['value'=>'顺丰速运'],
            'keyword2'=>['value'=>$order->snap_name,'color'=>'#274088'],
            'keyword3'=>['value'=>$order->order_no],
            'keyword4'=>['value'=>$dt->format('Y-m-d H:i:s')]
        ];
        $this->data = $data;
    }

    private function getUserOpenID($id)
    {
        $user = User::get($id);
        if(!$user){
            throw new UserException();
        }
        return $user->openid;
    }
}