<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/13 0013
 * Time: 下午 14:57
 */

namespace app\api\service;

use app\api\enum\OrderStatusEnum;
use app\api\model\Product;
use think\Db;
use think\Exception;
use think\Loader;
use app\api\service\Order as OrderService;
use app\api\model\Order as OrderModel;
use think\Log;

Loader::import('WxPay.WxPay',EXTEND_PATH,'.Api.php');
class WxNotify extends \WxPayNotify
{

    //调用微信的SDK,重写NotifyProcess方法，此方法作为对微信回调地址的结果的处理
    //使用事务来防止多次减少库存量，因为当高并发时，有可能第一次还没有更新订单的状态，而超过了第一次微信回调等待时间15秒，而发起第二次请求，两次都满足订单状态等于1，所以会造成多次减少库存
    public function NotifyProcess($data,&$msg)
    {
        if($data['result_code'] == 'SUCCESS')
        {
            $orderNO = $data['out_trade_no'];
            Db::startTrans();
            try{
                $order = OrderModel::where('order_no',$orderNO)->lock(true)->find();
                if($order->status == OrderStatusEnum::UNPAID)
                {
                    $orderService = new OrderService();
                    $stockStatus = $orderService->checkOrderStock($order->id);
                    if($stockStatus['pass'])
                    {
                        $this->updateOrderStatus($order->id,true);
                        $this->reduceStock($stockStatus);
                    }else
                    {
                        $this->updateOrderStatus($order->id,false);
                    }
                }
                Db::commit();
                return true;
            }catch (Exception $exception){
                Db::rollback();
                Log::error($exception);
                return false;
            }
        }else
        {
            //此时返回true是减少微信的回调，因为支付失败，微信会继续回调，直到3600秒以后，这里我们已经直到支付失败了，所以不用再让微信回调
            return true;
        }
    }

    //库存量充足时更新订单状态
    private function updateOrderStatus($orderId,$success)
    {
        $status = $success?OrderStatusEnum::PAID:OrderStatusEnum::PAID_BUT_OUT_OF;
        OrderModel::where('id',$orderId)->update(['status'=>$status]);
    }
    //库存量充足时更新商品数量
    private function reduceStock($stockStatus)
    {
        foreach ($stockStatus['pStatusArray'] as $singleProduct)
        {
            Product::where('id',$singleProduct['id'])->setDec('stock',$singleProduct['count']);
        }
    }
}