<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/23 0023
 * Time: 下午 14:27
 */

namespace app\api\service;


use think\Exception;

class WxMessage
{
    //模板推送连接
    private $url = '';
    private $touser;
    private $color = 'black';
    protected $tplID;
    protected $fromID;
    protected $page;
    protected $emphasisKeyWord;
    protected $data;

    function __construct()
    {
        $accessToken = new AccessToken();
        $token = $accessToken->get();
        $this->url = sprintf($this->url,$token);
    }

    //开发者工具中拉起微信支付的prepay_id是无效的，只有真机上的才有效
    public function sendMessage($openID)
    {
        $data = [
            'touser'=>$openID,
            'template_id'=>$this->tplID,
            'page'=>$this->page,
            'from_id'=>$this->fromID,
            'data'=>$this->data,
            'emphasis_keyword'=>$this->emphasisKeyWord
        ];
        $result = json_decode(curl_post($this->url,$data),true);
        if($result['errcode']==0){
            return true;
        }else{
            throw new Exception("模板消息发送失败".$result['errmsg']);
        }
    }
}