<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/29 0029
 * Time: 上午 9:31
 */

namespace app\api\service;


use app\api\enum\ScopeEnum;
use app\api\exception\ForbidenException;
use app\api\exception\TokenException;
use think\Cache;
use think\Request;
class Token
{
    public static function generateToken()
    {
        //第一组随机字符串32个字符
        $randChar = getRandChar();
        //第二组字符当前时间戳
        $times = $_SERVER['REQUEST_TIME'];
        //第三组随机字符串
        $salt = 'fqwhthtkyxnmrsfsdbty';
        return md5($randChar.$times.$salt);
    }

    //根据传入的不同参数获取不同的值，例如可以传入uid，sessionkey等等
    public static function getCurrentTokenVar($key)
    {
        //通过http头获取用户携带的令牌token
        $token = Request::instance()->header('token');
        //在缓存中读取先前存入的token
        $var = Cache::get($token);
        if(!$var)
        {
            throw new TokenException();
        }else
        {
            if(!is_array($var))
            {
                $var = json_decode($var,true);
            }
            if(!array_key_exists($key,$var))
            {
                exception('尝试获取的参数并不存在');
            }
            return $var[$key];
        }
    }

    //获取当前用户的uid
    public static function getCurrentUid()
    {
        return self::getCurrentTokenVar('uid');
    }

    //用户和管理员都能访问的接口权限
    public static function needPrimaryScope()
    {
        $scope = self::getCurrentTokenVar('scope');
        if(!isset($scope))
        {
            throw new TokenException();
        }
        if($scope < ScopeEnum::USER)
        {
            throw new ForbidenException();
        }
        return true;
    }
    //只有用户才能访问的接口权限
    public static function needExclusiveScope()
    {
        $scope = self::getCurrentTokenVar('scope');
        if(!isset($scope))
        {
            throw new TokenException();
        }
        if($scope != ScopeEnum::USER)
        {
            throw new ForbidenException();
        }
        return true;
    }

    //检查操作订单的是否是同一个人
    public static function isValidateOperate($checkedUID)
    {
        if(!$checkedUID)
        {
            exception("没有uid传入");
        }
        $currentUID = self::getCurrentUid();
        if($currentUID == $checkedUID)
        {
            return true;
        }
        return false;
    }

    //验证token合法性
    public static function verifyToken($token)
    {
        $result = Cache::get($token);
        if($result)
        {
            return true;
        }else
        {
            return false;
        }
    }
}