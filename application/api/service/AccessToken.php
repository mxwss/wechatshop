<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/23 0023
 * Time: 下午 14:59
 */

namespace app\api\service;


use think\Cache;
use think\Exception;

class AccessToken
{
    private $tokenUrl=''; //获取accessToken的连接
    const TOKEN_CACHED_KEY = 'access';
    const TOKEN_EXPIRE_IN = 7000;

    function __construct()
    {
        $url_original = config('setting.access_token_url');
        $url = sprintf($url_original,config('wx.app_id'),config('wx.app_secret'));
        $this->tokenUrl = $url;
    }

    public function get()
    {
        $token = Cache::get(self::TOKEN_CACHED_KEY);
        if(!$token){
            return $this->getFromWxServer();
        }else{
            return $token;
        }
    }

    //从微信服务器获取accessToken
    private function getFromWxServer()
    {
        $token = json_decode(curl_get($this->tokenUrl),true);
        if(!$token){
            throw new Exception("获取accessToken失败");
        }
        if(!empty($token['errcode'])){
            throw new Exception($token['errmsg']);
        }
        Cache::set(self::TOKEN_CACHED_KEY,$token,self::TOKEN_EXPIRE_IN);
        return $token['access_token'];
    }
}