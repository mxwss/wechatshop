<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/23 0023
 * Time: 上午 7:57
 */

namespace app\api\service;


use app\api\exception\TokenException;
use app\api\model\ThirdApp;

class AppToken extends Token
{
    //cms获取token
    public function get($ac,$se)
    {
        $app = ThirdApp::check($ac,$se);
        if(!$app){
            throw new TokenException("授权失败",10004);
        }
        $scope = $app->scope;
        $uid = $app->id;
        $values = [
            'scope'=>$scope,
            'uid'=>$uid
        ];
        $token = $this->saveToCache($values);
        return $token;
    }

    //生成token并将用户信息存入缓存
    private function saveToCache($value)
    {
        $token = self::generateToken();
        $expire_in = config('setting.token_expire_in');
        $result = cache($token,json_encode($value),$expire_in);
        if(!$result){
            throw new TokenException("服务器缓存异常",10005);
        }
        return $token;
    }
}