<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/5 0005
 * Time: 上午 11:10
 */

namespace app\api\exception;


class OrderException extends BaseException
{
    public $code = 404;
    public $message = '订单不存在，请检查订单id';
    public $errorCode = 80000;
}