<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/28 0028
 * Time: 上午 9:07
 */

namespace app\api\exception;


class BannerException extends BaseException
{
    public $code = 400;
    public $message = '请求的首页轮播不存在';
    public $errorCode = 40000;
}