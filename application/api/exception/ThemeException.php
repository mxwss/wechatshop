<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/28 0028
 * Time: 上午 10:47
 */

namespace app\api\exception;


class ThemeException extends BaseException
{
    public $code = 400;
    public $message = '请求的专题不存在';
    public $errorCode = 30000;
}