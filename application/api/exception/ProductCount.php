<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/28 0028
 * Time: 下午 13:37
 */

namespace app\api\exception;


class ProductCount extends BaseException
{
    public $code = 400;
    public $message = '请求的最新商品不存在';
    public $errorCode = 20000;
}