<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/29 0029
 * Time: 上午 9:48
 */

namespace app\api\exception;


class TokenException extends BaseException
{
    public $code = 401;
    public $message = 'token过期或不存在';
    public $errorCode = 10001;
}