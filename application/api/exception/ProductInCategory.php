<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/28 0028
 * Time: 下午 14:55
 */

namespace app\api\exception;


class ProductInCategory extends BaseException
{
    public $code = 400;
    public $message = '该栏目下还没有商品';
    public $errorCode = 50001;
}