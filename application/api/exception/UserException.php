<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/3 0003
 * Time: 上午 10:24
 */

namespace app\api\exception;


class UserException extends BaseException
{
    public $code = 404;
    public $message = '用户不存在';
    public $errorCode = 60000;
}