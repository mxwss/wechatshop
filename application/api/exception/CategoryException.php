<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/28 0028
 * Time: 下午 14:26
 */

namespace app\api\exception;


class CategoryException extends BaseException
{
    public $code = 400;
    public $message = '请求的分类目录不存在';
    public $errorCode = 50000;
}