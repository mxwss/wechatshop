<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/3 0003
 * Time: 下午 15:27
 */

namespace app\api\exception;


class ForbidenException extends BaseException
{
    public $code = 403;
    public $message = '没有权限访问';
    public $errorCode = 10001;
}