<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/28 0028
 * Time: 上午 9:06
 */

namespace app\api\exception;

use Exception;
class BaseException extends Exception
{
    public $code;
    public $message;
    public $errorCode;
}