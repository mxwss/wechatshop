<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/28 0028
 * Time: 上午 9:06
 */

namespace app\api\exception;

use Exception;
use think\exception\Handle;
use think\Log;
use think\Request;

class ExceptionHandler extends Handle
{
    public $code;
    public $message;
    public $errorCode;
    public function render(Exception $e)
    {
        if($e instanceof BaseException)
        {
            $this->code = $e->code;
            $this->message = $e->message;
            $this->errorCode = $e->errorCode;
        }else
        {
            if (config('app_debug'))
            {
                return parent::render($e);
            }
            $this->code = 500;
            $this->message = '系统内部错误';
            $this->errorCode = 99999;
            $this->recordLog($e);
        }
        $request = Request::instance();
        $data = [
            'msg'  => $this->message,
            'error_code' => $this->errorCode,
            'request_url' => $request = $request->url()
        ];
        return json($data, $this->code);
    }

    private function recordLog(Exception $e)
    {
        Log::init([
            'type'  =>  'File',
            'path'  =>  LOG_PATH,
            'level' => ['error']
        ]);
        Log::record($e->getMessage(),'error');
    }
}