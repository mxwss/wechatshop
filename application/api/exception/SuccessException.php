<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/3 0003
 * Time: 上午 11:14
 */

namespace app\api\exception;


class SuccessException extends BaseException
{
    public $code = 201;
    public $message = 'ok';
    public $errorCode = 0;
}