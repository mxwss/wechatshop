<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/28 0028
 * Time: 下午 16:37
 */

namespace app\api\exception;


use Throwable;

class WxExcepton extends BaseException
{
    public $code = 400;
    public $message = '微信openID请求失败';
    public $errorCode = 10004;
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}