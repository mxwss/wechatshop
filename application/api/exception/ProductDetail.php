<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/29 0029
 * Time: 上午 10:36
 */

namespace app\api\exception;


class ProductDetail extends BaseException
{
    public $code = 400;
    public $message = '请求的商品不存在';
    public $errorCode = 20000;
}