<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/3 0003
 * Time: 下午 15:21
 */

namespace app\api\enum;


class ScopeEnum
{
    const USER = 16;
    const SUPER = 32;
}