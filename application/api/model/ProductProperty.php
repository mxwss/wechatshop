<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/29 0029
 * Time: 上午 10:52
 */

namespace app\api\model;


class ProductProperty extends BaseModel
{
    protected $hidden = ['delete_time','update_time','product_id'];
    protected $autoWriteTimestamp = true;
    protected $createTime = false;
}