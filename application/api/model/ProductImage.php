<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/29 0029
 * Time: 上午 10:47
 */

namespace app\api\model;


class ProductImage extends BaseModel
{
    protected $hidden = ['img_id','delete_time','product_id'];
    protected $autoWriteTimestamp = true;
    protected $createTime = false;
    protected $updateTime = false;

    public function imgUrl()
    {
        return $this->belongsTo('Image','img_id','id');
    }
}