<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/28 0028
 * Time: 上午 9:36
 */

namespace app\api\model;


class Product extends BaseModel
{
    protected $hidden = ['create_time','delete_time','update_time','pivot','from'];
    protected $autoWriteTimestamp = true;
    //通过获取器来连接图片url，这张表做了冗余是为了提高数据查询性能，这里是为了提高图片查询的性能
    public function getMainImgUrlAttr($value,$data)
    {
        return $this->connectUrl($value,$data);
    }

    //商品和商品图片一对多
    public function productImgs()
    {
        return $this->hasMany('ProductImage','product_id','id');
    }

    public function productProperties()
    {
        return $this->hasMany('ProductProperty','product_id','id');
    }
    //获取最新的产品
    public static function getRecent($count)
    {
        return self::limit($count)->order('create_time desc')->select();
    }

    //获取分类栏目下的商品
    public static function getProductInCategory($id)
    {
        return self::where('category_id',$id)->select();
    }
    //获取单个商品详情
    public static function getProductDetail($id)
    {
        return self::with(['productImgs'=>function($query){
            $query->with('imgUrl')->order('order','asc');     //通过一个闭包函数来对关联的图片表进行排序
        }])->with('productProperties')->find($id);
    }
}