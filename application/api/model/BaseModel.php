<?php

namespace app\api\model;

use think\Model;

class BaseModel extends Model
{
    protected function connectUrl($value,$data)
    {
        $image_url = $value;
        //from等于1是本地图片，等于2是网络图片
        if($data['from']==1)
        {
            $image_url = config('setting.img_prefix').$value;
        }
        return $image_url;
    }
}
