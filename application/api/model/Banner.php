<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/27 0027
 * Time: 下午 12:21
 */

namespace app\api\model;


use think\Model;
class Banner extends BaseModel
{
    protected $hidden = ['update_time','delete_time'];
    protected $autoWriteTimestamp = true;
    protected $createTime = false;
    //返回首页banner内容
    public static function getBannerID($id)
    {
        return self::with(['bannerItems','bannerItems.images'])->find($id);
    }

    //banner表和banner_item表的一对多关联
    public function bannerItems()
    {
        return $this->hasMany('BannerItem','banner_id','id');
    }
}