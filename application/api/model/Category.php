<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/28 0028
 * Time: 下午 14:23
 */

namespace app\api\model;


class Category extends BaseModel
{
    protected $hidden = ['update_time','delete_time','topic_img_id'];
    protected $autoWriteTimestamp = true;
    protected $createTime = false;
    //关联图片表
    public function img()
    {
        return $this->belongsTo('Image','topic_img_id','id');
    }

    //返回分类目录结果
    public static function getCategories()
    {
        return self::all([],'img');
    }
}