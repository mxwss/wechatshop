<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/23 0023
 * Time: 上午 8:09
 */

namespace app\api\model;


class ThirdApp extends BaseModel
{
    //从数据库查找对应cms管理员信息
    public static function check($ac,$se)
    {
        $app = self::where('app_id',$ac)
            ->where('app_secret',$se)
            ->find();
        return $app;
    }
}