<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/28 0028
 * Time: 下午 15:51
 */

namespace app\api\model;


class User extends BaseModel
{
    protected $autoWriteTimestamp = true;
    public function address()
    {
        return $this->hasOne('UserAddress','user_id','id');
    }
    public static function selectUserByToken($openid)
    {
        return self::where('openid',$openid)->find();
    }
}