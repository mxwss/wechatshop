<?php

namespace app\api\model;

use think\Model;

class Image extends BaseModel
{
    protected $hidden = ['id','update_time','delete_time','from'];
    protected $autoWriteTimestamp = true;
    protected $createTime = false;
    //通过获取器完成image图片url路径的拼接
    public function getUrlAttr($value,$data)
    {
        return $this->connectUrl($value,$data);
    }
}
