<?php

namespace app\api\model;

use think\Model;

class BannerItem extends BaseModel
{
    protected $table = 'Banner_item';
    protected $hidden = ['id','update_time','delete_time','img_id','banner_id'];
    protected $autoWriteTimestamp = true;
    protected $createTime = false;

    //实现banner_item表和image表的一一对应关系
    public function images()
    {
        return $this->belongsTo('Image','img_id','id');
    }
}
