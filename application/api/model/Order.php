<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/7/10 0010
 * Time: 上午 10:15
 */

namespace app\api\model;


class Order extends BaseModel
{
    protected $hidden = ['user_id','delete_time','update_time'];
    protected $autoWriteTimestamp = true;

    //对snapitems字段反序列化
    public function getSnapItemsAttr($value)
    {
        if(empty($value))
        {
            return null;
        }
        return json_decode($value);
    }
    //对snapaddress字段反序列化
    public function getSnapAddressAttr($value)
    {
        if(empty($value))
        {
            return null;
        }
        return json_decode($value);
    }
    //根据时间降序排序对历史订单分页
    public static function getSummaryByUser($uid,$page=1,$size=15)
    {
        $data = self::where('user_id',$uid)
            ->order('create_time desc')
            ->paginate($size,true,['page'=>$page]);
        return $data;
    }

    //获取全部订单
    public static function getSummaryByPage($page,$size)
    {
        $data = self::order('create_time desc')
            ->paginate($size,true,['page'=>$page]);
        return $data;
    }
}