<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/6/28 0028
 * Time: 上午 9:35
 */

namespace app\api\model;


class Theme extends BaseModel
{
    protected $hidden = ['update_time','delete_time','topic_img_id','head_img_id'];
    protected $autoWriteTimestamp = true;
    protected $createTime = false;
    //首页专题图片和图片表一对一
    public function topicImage()
    {
        return $this->belongsTo('Image','topic_img_id','id');
    }

    //专题详情页的背景图和图片表一对一
    public function headImage()
    {
        return $this->belongsTo('Image','head_img_id','id');
    }
    //实现专题和产品多对多
    public function products()
    {
        return $this->belongsToMany('Product','theme_product','product_id','theme_id');
    }
    //得到首页专题详情
    public static function getTheme($id)
    {
        return self::with('topicImage,headImage')->select($id);
    }

    //得到专题详情页内容
    public static function getThemeData($id)
    {
        return self::with('products,topicImage,headImage')->find($id);
    }



}